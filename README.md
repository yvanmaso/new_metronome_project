# AOC TP #

Cette application nous montre le fonctionnement d'un métronome avec le langage JavaFx suivant le cours d'AOC

###Code source et Repertoires ###

* dashboard:source de boutons et slider
* engine:moteur de l'application servant comme modele*
* controller:controlleurs des commandes
* command:commandes concrètes et interface command
* ihm:main de l'application 
 

### Implémentation ###

* lancer le main.java de l'IHM/main.jav 

### Contributeur###

* Yvan Christian Maso
