package vue;

import controller.Controller;
import ihm.IHMInterface;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 20/02/2016.
 */
public class View implements IView
{
    private IHMInterface ihm;

    public View(IHMInterface ihm) {
        this.ihm = ihm;
    }

    public IHMInterface getIhm() {
        return ihm;
    }

    public void setIhm(IHMInterface ihm) {
        this.ihm = ihm;
    }

    public void setController(Controller controller)
    {
        ihm.setController(controller);

    }

    public void flashLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        ihm.flashLed1();
    }

    public void flashLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        ihm.flashLed2();
    }

    public void start() {

        ihm.start();
    }

    public void stop() {
    ihm.stop();    }

    public void inc() {
        ihm.inc();
    }

    public void dec() {
    ihm.dec();
    }
}
