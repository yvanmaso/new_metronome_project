package vue;

import command.Command;
import javafx.application.Platform;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by yvan on 20/02/2016.
 */
public class Horloge implements IHorloge
{
    private Timer timer = new Timer();
    private Map<Command, TimerTask> tasks = new HashMap<Command, TimerTask>();

    public void periodicallyActivate(final Command cmd, double perdiodInMiliSeconds)
    {
        TimerTask task = new TimerTask() {

            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        try {
                            cmd.execute();
                        } catch (UnsupportedAudioFileException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (LineUnavailableException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        if (cmd != null)
            timer.schedule(task, 0, (long) perdiodInMiliSeconds);

        tasks.put(cmd, task);
    }

    public void desactivate(Command cmd) {

        if(!tasks.isEmpty())
            tasks.get(cmd).cancel();
    }
}
