package vue;

import controller.Controller;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 20/02/2016.
 */
public interface IView
{
    void setController(Controller controller);

    void flashLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException;

    void flashLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException ;

    void start();

    void stop();

    void inc();

    void dec();

}
