package vue;

import command.Command;

/**
 * Created by yvan on 20/02/2016.
 */
public interface IHorloge
{
    public void periodicallyActivate(Command cmd, double perdiodInMiliSeconds);
    public void desactivate(Command cmd);
}
