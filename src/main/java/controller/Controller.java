package controller;

import command.BarCommand;
import command.BeatCommand;
import engine.Engine;
import engine.IEngine;
import ihm.IHMInterface;
import vue.IView;
import vue.View;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public class Controller implements IController
{
    private IEngine engine;

    private IView view;

    private static IController controller = null;

    public static IController getControllerInstance(){
        if(controller == null) controller = new Controller();
        return controller;
    }


    public Controller()
    {
        engine= Engine.getControllerInstance();
    }

    public IEngine getEngine() {
        return engine;
    }

    public void setEngine(IEngine engine) {
        this.engine = engine;
    }

    public IView getView() {
        return view;
    }

    public void setView(IView view) {
        this.view = view;
    }

    public static IController getController() {
        return controller;
    }

    public static void setController(IController controller) {
        Controller.controller = controller;
    }

    /**
     * demarrer le metronome
     */
    public void start()
    {
        if(view != null)
            view.setController(this);
        if (!engine.isRunning()) {

            BeatCommand beatCommand = new BeatCommand();
            beatCommand.setController(this);
            engine.setBeatCmd(beatCommand);

            BarCommand barCommand = new BarCommand();
            barCommand.setController(this);
            engine.setBarCmd(barCommand);
            engine.setRunning(true);
        }
    }

    public void stop()
    {

    engine.setRunning(false);
    }

    public String initBPM()
    {
        return IEngine.BPM_INITIAL_VALUE +"";
    }

    public void inc() {

        engine.incNTM();
    }

    public void dec() {
        engine.decNTM();
    }

    public void display() {

    }

    public void createViewAdapter(IHMInterface ihmController) {
        view=new View(ihmController);
    }

    public void displayLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
    view.flashLed1();
    }

    public void displayLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
    view.flashLed2();
    }

    public void notifyMetronomeSlidePosition(int position) {

        engine.setTempo(position);
    }
}
