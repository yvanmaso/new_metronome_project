package controller;

import ihm.IHMInterface;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public interface IController
{
    /**
     * demarrer le metronome
     */
    public void start();
    /*
     * Stopper le metronome
     */
    public void stop();
    /*
     * initialization du bpm
     */
    public String initBPM();
    /*
     * Incrementer le BPM
     */
    public void inc();

    /*
     * decrementer le BPM
     */
    public void dec();
    public void display();
	/*
	 * creer l'adapteur
	 */
    public void createViewAdapter(IHMInterface ihmController);

    /*
     * disables and stops the led1
     */
    public void displayLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException;

    public void displayLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException;

    void notifyMetronomeSlidePosition(int position);


}
