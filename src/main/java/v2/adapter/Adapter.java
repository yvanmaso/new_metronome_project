package v2.adapter;

import controller.IController;
import ihm.IHMInterface;

/**
 * Created by yvan on 22/02/2016.
 */
public interface Adapter extends IController, IHMInterface {
    void lireMateriel();
}
