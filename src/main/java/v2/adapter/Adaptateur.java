package v2.adapter;

import controller.Controller;
import controller.IController;
import ihm.IHMInterface;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 22/02/2016.
 */
public class Adaptateur implements Adapter {
    private IController controleur;
    private IHMInterface ihm;


    public Adaptateur() {
    }


    public IController getControleur() {
        return controleur;
    }

    public void setControleur(IController controleur) {
        this.controleur = controleur;
    }

    public IHMInterface getIhm() {
        return ihm;
    }

    public void setIhm(IHMInterface ihm) {
        this.ihm = ihm;
    }

    @Override
    public void lireMateriel() {

    }

    /**
     * demarrer le metronome
     */
    @Override
    public void start() {
controleur.start();
    }

    @Override
    public void stop() {
controleur.stop();
    }

    @Override
    public String initBPM() {
        return controleur.initBPM();
    }

    @Override
    public void inc() {
controleur.inc();
    }

    @Override
    public void dec() {
controleur.dec();
    }

    @Override
    public void display() {
controleur.display();
    }

    @Override
    public void createViewAdapter(IHMInterface ihmController) {
controleur.createViewAdapter(ihmController);
    }

    @Override
    public void displayLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        controleur.displayLed1();
    }

    @Override
    public void displayLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
    controleur.displayLed2();
    }

    @Override
    public void flashLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        ihm.flashLed1();
    }

    @Override
    public void flashLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
ihm.flashLed2();
    }

    @Override
    public void setController(Controller controller) {
        this.controleur=controller;
    }

    @Override
    public void notifyMetronomeSlidePosition(int position) {

    }
}
