package dashboard;

import command.Command;
import javafx.scene.control.Button;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public class BoutonStart implements IButton
{
    Button start;
    Command commandStart;

    public BoutonStart(Button start) {
        this.start = start;
    }

    public void setClickCommand(Command parameter) {
        commandStart=parameter;
        try {
            commandStart.execute();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }
}
