package engine;

import command.Command;
import vue.Horloge;

/**
 * Created by yvan on 23/12/2015.
 */
public class Engine implements IEngine
{

    private Integer bpm;

    /* nombre de temps dans une mesure */
    private Integer ntm;

    private Horloge horlogeBar;
    private Horloge horlogeBeat;

    private boolean running;

    private Command barCmd;
    private Command beatCmd;

    static IEngine instance = null;


    public Engine()
    {
        super();
        bpm = BPM_INITIAL_VALUE;
        ntm = NTM_INITIAL_VALUE;
        initHorloge();
    }

    private void initHorloge() {

        // Set up
        horlogeBar = new Horloge();
        horlogeBeat = new Horloge();

    }


    public boolean isRunning() {
        return running;
    }

    public Integer getTempo() {
        return bpm;
    }

    public Integer getNtm() {
        return ntm;
    }

    public void setNtm(Integer ntm)
    {
        this.ntm = ntm;
        initHorloge();
        setRunning(false);
        setRunning(true);
    }

    public void setBarCmd(Command parameter) {
        barCmd=parameter;

    }

    public void setBeatCmd(Command parameter) {
    beatCmd=parameter;
    }

    public void setRunning(boolean parameter)
    {

        this.running = running;
        if (running) {
            double delayStep = (60.0 / bpm) * 1000;
            horlogeBeat.periodicallyActivate(beatCmd, delayStep);
            horlogeBar.periodicallyActivate(barCmd, delayStep * ntm);
        } else
        {
            horlogeBar.desactivate(barCmd);
            horlogeBeat.desactivate(beatCmd);
        }
    }

    public void setTempo(int parameter) {

        this.bpm = parameter;
        initHorloge();
        setRunning(false);
        setRunning(true);
    }

    public void incNTM() {
        if (ntm < NTM_MAX_VALUE) {
            ntm++;

            // restart of the metronome
            initHorloge();

            setRunning(false);
            setRunning(true);
        }
    }

    public void decNTM() {
        if (ntm > NTM_MIN_VALUE) {
            ntm--;
            // restart of the metronome
            initHorloge();

            setRunning(false);
            setRunning(true);
        }
    }

    public String getBPM() {
        return bpm + "";
    }

    public static IEngine getControllerInstance() {

        if(instance == null) instance = new Engine();
        return instance;
    }
}
