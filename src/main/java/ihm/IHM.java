package ihm;

import adapter.*;
import command.*;
import controller.Controller;
import engine.Engine;
import javafx.animation.FillTransition;
import javafx.animation.TranslateTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by yvan on 23/12/2015.
 */
public class IHM implements IHMInterface,Initializable
{

    @FXML
    private Button start;

    @FXML
    private Button stop;

    @FXML
    private Button inc;

    @FXML
    private Button dec;

    @FXML
    private Circle led1;

    @FXML
    private Circle led2;

    @FXML
    private TextField textfield;

    @FXML
    private TextField textfield1;

    /* Molette */
    @FXML
    private Slider slider;

    private IButton startBtn;
    private IButton stopBtn;
    private IButton incBtn;
    private IButton decBtn;

    // Controller
    private Controller controller;



    TranslateTransition transition;
    boolean theFistTimeBeat = true;

    AudioInputStream audioInputStream;
    Clip clip;
    private boolean theFistTimeBar=true;


    public IHM()
    {

    }

    public Button getStart() {
        return start;
    }

    public void setStart(Button start) {
        this.start = start;
    }

    public Controller getController() {
        return controller;
    }

    public IButton getDecBtn() {
        return decBtn;
    }

    public void setDecBtn(IButton decBtn) {
        this.decBtn = decBtn;
    }

    public IButton getIncBtn() {
        return incBtn;
    }

    public void setIncBtn(IButton incBtn) {
        this.incBtn = incBtn;
    }

    public IButton getStopBtn() {
        return stopBtn;
    }

    public void setStopBtn(IButton stopBtn) {
        this.stopBtn = stopBtn;
    }

    public IButton getStartBtn() {
        return startBtn;
    }

    public void setStartBtn(IButton startBtn) {
        this.startBtn = startBtn;
    }

    public Slider getSlider() {
        return slider;
    }

    public void setSlider(Slider slider) {
        this.slider = slider;
    }

    public TextField getTextfield1() {
        return textfield1;
    }

    public void setTextfield1(TextField textfield1) {
        this.textfield1 = textfield1;
    }

    public TextField getTextfield() {
        return textfield;
    }

    public void setTextfield(TextField textfield) {
        this.textfield = textfield;
    }

    public Circle getLed2() {
        return led2;
    }

    public void setLed2(Circle led2) {
        this.led2 = led2;
    }

    public Circle getLed1() {
        return led1;
    }

    public void setLed1(Circle led1) {
        this.led1 = led1;
    }

    public Button getDec() {
        return dec;
    }

    public void setDec(Button dec) {
        this.dec = dec;
    }

    public Button getInc() {
        return inc;
    }

    public void setInc(Button inc) {
        this.inc = inc;
    }

    public Button getStop() {
        return stop;
    }

    public void setStop(Button stop) {
        this.stop = stop;
    }

    public void flashLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException
    {
        if (!theFistTimeBeat) {
            FillTransition st = new FillTransition(Duration.millis(100), led1,
                    Color.WHITE, Color.RED);
            st.setCycleCount(4);
            st.setAutoReverse(true);

            st.play();

            audioInputStream = AudioSystem.getAudioInputStream(new File(
                    "resources/beep.wav").getAbsoluteFile());

            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();

        } else
            theFistTimeBeat = false;
    }

    public void flashLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        if (!theFistTimeBar) {
            FillTransition st = new FillTransition(Duration.millis(100), led2,
                    Color.WHITE, Color.RED);
            st.setCycleCount(4);

            st.setAutoReverse(true);

            st.play();

            audioInputStream = AudioSystem.getAudioInputStream(new File(
                    "resources/bar.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();

        } else
            theFistTimeBar = false;
    }

    @Override
    public void setController(Controller controller) {
        this.controller=controller;

    }

    public void notifyMetronomeSlidePosition(int position)
    {
        SlideCommand slideCmd = new SlideCommand();
        slideCmd.setController(controller);
        slideCmd.setPosition(position);
        start.setOnAction((e) -> {
            try {
                slideCmd.execute();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (LineUnavailableException e1) {
                e1.printStackTrace();
            }
        });
    }

    public void start() {
        StartCommand startCmd = new StartCommand();
        startCmd.setController(controller);
        start.setOnAction((e) -> {
            startCmd.execute();
        });

    }

    public void stop() {
        StopCommand stopCmd = new StopCommand();
        stopCmd.setController(controller);
        theFistTimeBar = true;
        theFistTimeBeat = true;
        stop.setOnAction((e) -> {
            try {
                stopCmd.execute();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (LineUnavailableException e1) {
                e1.printStackTrace();
            }
            led1.setFill(Color.WHITE);
            led2.setFill(Color.WHITE);
        });

    }

    public void inc() {
        IncCommand incCmd = new IncCommand();
        incCmd.setController(controller);
        inc.setOnAction((e) -> {
            try {
                incCmd.execute();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (LineUnavailableException e1) {
                e1.printStackTrace();
            }
        });

    }

    public void dec() {

        DecCommand decCmd = new DecCommand();
        decCmd.setController(controller);

        dec.setOnAction((e) -> {
            try {
                decCmd.execute();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (LineUnavailableException e1) {
                e1.printStackTrace();
            }
        });

    }

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    public void initialize(URL location, ResourceBundle resources) {

        Logger.getGlobal().info(
                String.format("Initialized with a button start",
                        start.toString()));

        // BPM Initialisation
        textfield.setText(String.valueOf(Engine.BPM_INITIAL_VALUE));

        setStartBtn(new BoutonStart(start));
        setStopBtn(new BoutonStop(stop));
        setIncBtn(new BoutonInc(inc));
        setDecBtn(new BoutonDec(dec));
        controller = (Controller) Controller.getControllerInstance();
        controller.createViewAdapter(this);
        start();
        stop();
        inc();
         dec();
        final IHM pere = this;
        // Slider
        slider.setMin(Engine.BPM_MIN_VALUE);
        slider.setMax(Engine.BPM_MAX_VALUE);
        slider.setValue(Engine.BPM_INITIAL_VALUE);
        slider.valueProperty().addListener((ov, old_val, new_val) -> {
            int position = (int) new_val.doubleValue();
            textfield.setText(position + "");
            pere.notifyMetronomeSlidePosition(position);
        });

    }
}
