package ihm;

import controller.Controller;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public interface IHMInterface
{

    void flashLed1() throws UnsupportedAudioFileException, IOException, LineUnavailableException;
    void flashLed2() throws UnsupportedAudioFileException, IOException, LineUnavailableException ;
    void setController(Controller controller);
    public void notifyMetronomeSlidePosition(int position) ;
    void start();
    void stop();

    void inc();
    void dec();

}
