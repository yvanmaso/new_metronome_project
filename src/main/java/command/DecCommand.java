package command;

import controller.IController;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public class DecCommand implements Command{

    IController controller;

    public DecCommand() {
    }

    public DecCommand(IController controller) {
        this.controller = controller;
    }

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

    public void execute() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

        controller.dec();
    }
}
