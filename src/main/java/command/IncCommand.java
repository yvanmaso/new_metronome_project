package command;

import controller.IController;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public class IncCommand implements Command
{
    IController controller;

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

    public IncCommand() {

    }

    public void execute() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        controller.inc();
    }
}
