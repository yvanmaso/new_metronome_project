package command;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public interface Command
{
    void execute() throws UnsupportedAudioFileException, IOException, LineUnavailableException;
}
