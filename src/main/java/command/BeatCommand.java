package command;

import controller.IController;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 20/02/2016.
 */
public class BeatCommand implements Command {

    IController controller;

    public BeatCommand() {
    }

    public BeatCommand(IController controller) {
        this.controller = controller;
    }

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

    public void execute()
    {
        try {
            controller.displayLed1();
        }
        catch (UnsupportedAudioFileException e)
        {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }

    }
}
