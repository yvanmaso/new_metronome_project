package command;

import controller.IController;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015 c.
 */
public class SlideCommand  implements Command{

    IController controller;
    private int position = 0;

    public SlideCommand(IController controller) {
        this.controller = controller;
    }

    public SlideCommand() {
    }

    public IController getController() {
        return controller;
    }

    public void setController(IController controller) {
        this.controller = controller;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void execute() throws UnsupportedAudioFileException, IOException, LineUnavailableException
    {

        controller.notifyMetronomeSlidePosition(position);

    }
}
