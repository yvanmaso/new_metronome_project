package command;

import controller.Controller;
import org.omg.CORBA.CODESET_INCOMPATIBLE;

/**
 * Created by yvan on 23/12/2015.
 */
public class StartCommand implements Command
{

    private Controller controller;

    public StartCommand(Controller controller) {
        this.controller = controller;
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public StartCommand() {
    }

    public void execute() {

        controller.start();
    }
}
