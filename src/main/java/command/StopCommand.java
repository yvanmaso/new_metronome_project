package command;

import controller.Controller;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 23/12/2015.
 */
public class StopCommand implements Command
{
    private Controller controller;

    public StopCommand() {
    }

    public StopCommand(Controller controller) {

        this.controller = controller;
    }

    public void execute() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        controller.stop();
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
