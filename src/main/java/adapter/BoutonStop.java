package adapter;

import command.Command;
import javafx.scene.control.Button;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public class BoutonStop implements IButton
{
    Button stop;
    Command commandStop;

    public BoutonStop(Button stop) {
        this.stop = stop;
    }

    public void setClickCommand(Command parameter) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        commandStop=parameter;
        commandStop.execute();
    }
}
