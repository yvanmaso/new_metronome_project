package adapter;

import command.Command;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public interface IButton
{
    public void setClickCommand(Command parameter) throws UnsupportedAudioFileException, IOException, LineUnavailableException;
}
