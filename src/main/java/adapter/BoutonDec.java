package adapter;

import command.Command;
import javafx.scene.control.Button;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public class BoutonDec implements IButton
{

    Button dec;
    Command commandDec;

    public BoutonDec(Button dec) {
        this.dec = dec;
    }

    public void setClickCommand(Command parameter)
    {
        commandDec=parameter;
        try {
            commandDec.execute();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }


    }
}
