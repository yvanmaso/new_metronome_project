package adapter;

import command.Command;
import javafx.scene.control.Button;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public class SliderAdapter implements IButton
{
    Button dec;

    Command commandDec;

    public SliderAdapter() {
    }

    public SliderAdapter(Button dec) {
        this.dec = dec;
    }

    public void setClickCommand(Command parameter) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        commandDec=parameter;
        commandDec.execute();;
    }
}
