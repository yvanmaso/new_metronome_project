package adapter;

import command.Command;
import javafx.scene.control.Button;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

/**
 * Created by yvan on 04/01/2016.
 */
public class BoutonInc implements IButton
{
    Button inc;
    Command commandInc;

    public BoutonInc(Button inc) {
        this.inc = inc;
    }

    public void setClickCommand(Command parameter) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
commandInc=parameter;
        commandInc.execute();
    }
}
